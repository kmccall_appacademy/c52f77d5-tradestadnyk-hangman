class HumanPlayer
  def initialize
    @guessed_letters = []
  end

  def register_secret_length(length)
    puts "The secret word has length of #{length}"
  end

  def handle_response(guess, board)

  end

  def guess(board)
    print_board(board)
    print "> "
    guess = gets.chomp.downcase
    process_guess(guess)
  end

  def pick_secret_word
    puts "Think of a secret word. Enter its length: "
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts "The computer has guessed the letter #{letter}"
    print "Positions (ex 0 2 4): "
    indices = gets.chomp.split.map(&:to_i)
  end
  private

  def process_guess(guess)
    unless @guessed_letters.include?(guess)
      @guessed_letters << guess
      return guess
    else
      puts "You already guessed #{guess}"
      puts "You have already guessed:"
      p @guessed_letters
      puts "Please guess again"
      print "> "
      guess = gets.chomp.downcase
      process_guess(guess)
    end
  end

  def print_board(board)
    board_string = board.map do |el|
      el.nil? ? "_" : el
    end.join('')
    puts "Secret word: #{board_string}"
  end
end

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = ""
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  private

  def update_board(indices, letter)
    indices.each {|idx| @board[idx] = letter}
  end

  def over?
    @board.count(nil) == 0
  end

  def conclude
    puts "Congratulations! You guessed a word. It was #{@board.join('')}"
  end


end

class ComputerPlayer
  attr_reader :dictionary

    def initialize(dictionary)
      @dictionary = dictionary
      @alphabet = ('a'..'z').to_a
    end

    def candidate_words
      @dictionary
    end

    def self.read_dictionary
      File.readlines('dictionary.txt').map(&:chomp)
    end

    def pick_secret_word
      @secret_word = @dictionary.sample
      @secret_word.length
    end

    def check_guess(letter)
      indices = []
      @secret_word.each_char.with_index do |char, idx|
        indices << idx if char == letter
      end
      indices
    end

    def register_secret_length(length)
      @known_letters = Array.new(length)
      @dictionary.select! {|word| word.length == length}
    end

    def handle_response(letter, indices)
      @dictionary.select! do |word|
        word_indices = []
        word.chars.each_with_index do |char, idx|
          word_indices << idx if char == letter
        end
        indices == word_indices
      end
    end

    def guess(board)
      best = @alphabet.first
      best_count = 0
      @alphabet.each do |letter|
        count = @dictionary.count {|word| word.include? letter}
        if count > best_count
          best = letter
          best_count = count
        end
      end
      best
    end

end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {
    referee: HumanPlayer.new,
    guesser: ComputerPlayer.new(dictionary)
  }
  game = Hangman.new(players)
  game.play
end
